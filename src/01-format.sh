LOG_LEVEL="${LOG_LEVEL:-4}"
UTIL_PROGRAM="$(basename -- "$0")"
UTIL_POFFSET="$(echo -n "$prog" | sed 's/./ /g')"

fmt_tty() {
    while [ $# -gt 1 ]
    do
        local i
        for i in $(echo -n "$1" | sed -e 's/\(.\)/\1\n/g')
        do
            case "$i" in
                (h) tput bold    ;;
                (u) tput smul    ;;
                (r) tput setaf 1 ;;
                (g) tput setaf 2 ;;
                (y) tput setaf 3 ;;
                (b) tput setaf 4 ;;
            esac
        done
        shift
    done
    echo -n "$@"
    tput sgr0
}

fmt() {
    local str
    local arg
    local opt
    local echoopt
    local noopt=false
    while [ $# -gt 0 ]
    do
        arg="$1"
        if "$noopt"; then
            str="${str}${@}"
            break
        else
            case "$arg" in
                (--) noopt=true ;;
                (-n) echoopt="${echoopt} -n" ;;
                (-e) echoopt="${echoopt} -e" ;;
                (-*) opt="$(echo -n "$arg" | sed -ne 's/^-\(.\+\)$/\1/p')" ;;
                (*)
                    if [ "$opt" ] && [ -t 1 ]; then
                        str="${str}$(fmt_tty "$opt" "$arg")"
                    else
                        str="${str}${arg}"
                    fi
                    opt=
                    ;;
            esac
        fi
        shift
    done
    echo $echoopt "$str"
}

debug_p() { [ "$LOG_LEVEL" -ge 4 ] && fmt >&2 "$@";                           }
debug()   { [ "$LOG_LEVEL" -ge 4 ] && fmt >&2 "${UTIL_PROGRAM}: " -hg 'debug' ': ' "$@"; }
info_p()  { [ "$LOG_LEVEL" -ge 3 ] && fmt >&2 "$@";                           }
info()    { [ "$LOG_LEVEL" -ge 3 ] && fmt >&2 "${UTIL_PROGRAM}: " -hg 'info' ': ' "$@";  }
warn_p()  { [ "$LOG_LEVEL" -ge 2 ] && fmt >&2 "$@";                           }
warn()    { [ "$LOG_LEVEL" -ge 2 ] && fmt >&2 "${UTIL_PROGRAM}: " -hy 'warn' ': ' "$@";  }

error_p() {
    [ "$LOG_LEVEL" -ge 1 ] && fmt >&2 "$@"
    false
}
error() {
    [ "$LOG_LEVEL" -ge 1 ] && fmt >&2 "${UTIL_PROGRAM}: " -hr 'error' ': ' "$@"
    false
}

fatal() {
    local ec="$1"
    shift
    [ "$LOG_LEVEL" -ge 1 ] && fmt >&2 "${UTIL_PROGRAM}: " -hr 'fatal' ': ' "$@"
    exit "$ec"
}
