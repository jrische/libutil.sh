getv() {
    eval 'echo -n "$'"$1"'"'
}

setv() {
    eval "${1}='$(echo -n "$2" | sed "s/'/'\\\\''/g")'"
}

are_perms() {
    [ "$(stat -c '%a' "$2")" = "$1" ]
}

# -m : print missing tools
has_tools() {
    local missing
    local show=false
    for i in "$@"
    do
        if [ "$i" = '-m' ]; then
            show=true
        else
            command -v "$i" >/dev/null || missing="${missing} ${i}"
        fi
    done
    "$show" && echo -n $missing
    [ ! "$missing" ]
}

check_tools() {
    local missing
    missing="$(has_tools -m "$@")" || fatal 1 "tool(s) missing to run this command:" -h "$missing"
}

try() {
    local output
    output="$("$@")" \
        && { info_p success; [ "$output" ] && info -- "$output"; true; } \
        || { info_p failure; error -- "$output"; }
}

push() {
    if [ $# -ge 2 ]; then
        local var="$1"
        shift
        local val="$(getv "$var")"
        if [ "$val" ]
            then setv "$var" "${val} ${@}"
            else setv "$var" "$@"
        fi
    fi
}

contain() {
    local v="$1"
    shift
    for i in "$@"
    do
        [ "$i" = "$v" ] && return 0
    done
    false
}
